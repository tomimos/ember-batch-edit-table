import Component from '@ember/component';
import { computed } from '@ember/object';
import { A } from '@ember/array';
import EmberObject from '@ember/object';
import {
  bindKeyboardShortcuts,
  unbindKeyboardShortcuts
} from 'ember-keyboard-shortcuts';

export default Component.extend({
  init() {
    this._super(...arguments);
    this.set("defaultRowData", this.get("rowData"));
    this.set("defaultBulkRowData", this.get("headerInputs"));
  },

  keyboardShortcuts: {
    enter: {
      action: function (e){
        this.keyPressed(e.target)
      }      , // action to trigger
      global: true, // whether to trigger inside input (default: true)
      preventDefault: false // (default: true)
    }
  },

  didInsertElement() {
    this._super(...arguments);
    bindKeyboardShortcuts(this);
  },

  willDestroyElement() {
    this._super(...arguments);
    unbindKeyboardShortcuts(this);
  },

  keyPressed(target) {
    var tar = this.$(target);
    if (!tar.is( "input" )){
      return true
    }

    var id = target.id;
    var isSelect = false;

    if (!id){
      var tar = tar.parent().parent().prev("select");
      var tid = tar.attr("id");
      if (tar.length && tid){
        id = tid;
        isSelect = true;
      } else {
        return true;
      }
    }

    var split = id.split("_");
    split.pop();
    var searchString = "[id^=" + split.join("_") + "]:enabled";

    var closest = tar.closest("tr");
    var next = closest.next("tr");
    var found;
    while (next.length > 0 && !found){
      var find = next.find(searchString);
      if (find.length > 0){
        found = true;
        find.focus();
        if (isSelect){
          find.next().find("input").click();
        }
      } else {
        next = next.next("tr")
      }
    }
    return true;
  },
  headerInputModel: "headerInput",

  dateFormat: "MM/DD/YYYY",

  didReceiveAttrs(){
    this._super(...arguments);
    this.set("defaultRowData", this.get("rowData"));
    this.set("defaultBulkRowData", this.get("headerInputs"));
  },
  allLocked: computed('rowData.@each.locked', function () {
    var rowData = this.get('rowData');
    var unlockedRows = rowData.filter(function (item) {
      return item.get('locked') !== true;
    })
    return unlockedRows.length === 0 && !this.get('initialLoad');
  }),

  allUnlocked: computed('rowData.@each.locked', function () {
    var rowData = this.get('rowData');
    var lockedRows = rowData.filter(function (item) {
      return item.get('locked') === true;
    })
    return lockedRows.length === 0 && !this.get('initialLoad');
  }),

  unlockAllRows: function () {
    var rowData = this.get('rowData');
    rowData.forEach(function (item) {
      item.set('locked', false);
    });
    this.updateData(rowData);
  },

  lockAllRows: function () {
    if (this.get("unlockOnly")) {
      return;
    }
    var rowData = this.get('rowData');
    rowData.forEach(function (item) {
      item.set('locked', true);
    });

    this.updateData(rowData);
  },
  performBulkUpdate(){
    var bulkUpdateValues = this.get("bulkUpdateValues")
    var key = bulkUpdateValues.get("key")
    var value = bulkUpdateValues.get("value");
    var action = bulkUpdateValues.get("action");

    var lockable = this.get('lockable')
    var rowData = this.get('rowData');
    if (action) {
      action(value)
    } else {
      rowData.forEach(function (item) {
        if (!lockable || !item.get('locked')) {
          var rowCell = item.get(key);
          rowCell.set("value", value);
        }
      });
    }
    this.set("defaultRowData", this.get("rowData"));
    this.updateData(this.get("rowData"));
  },
  bulkUpdate: function (key, value, action, requiresConfirmation, bulkConfirmationConfig) {
    var object = EmberObject.extend({});
    bulkConfirmationConfig = bulkConfirmationConfig ? bulkConfirmationConfig : object.create({})
    var bulkValues = object.create({
      'key': key,
      'value': value,
      'action': action
    });
    this.set("bulkUpdateValues", bulkValues);
    if (requiresConfirmation) {
      this.set("bulkValueUpdateMessage", bulkConfirmationConfig.getWithDefault("messageContent",  "Are you sure you want to update this value?"))
      this.set("bulkValueConfirmationConfig", bulkConfirmationConfig);
      this.showBulkUpdateDialog()
    } else {
      this.performBulkUpdate()
    }
  },
  revertToDefaultRowValues: function () {
    var defaultRowData = this.get("storedDefaultRowValues");
    var rowData = this.get("rowData");
    var columns = this.get("columns");

    defaultRowData.forEach(function (defaultRow, index) {
      var row = rowData[index];
      columns.forEach(function (column) {
        var rowInput = column.get('input');
        var cell = row.get(rowInput);
        var defaultValue = defaultRow.get(rowInput);
        cell.set("value", defaultValue);
      });
    });
  },
  revertToDefaultBulkRowValues: function () {
    var defaultRowData = this.get("defaultBulkRowData");
    var headerInputs = this.get("headerInputs");

    defaultRowData.forEach(function (defaultHeaderInput, index) {
      var value = defaultHeaderInput.get("value");
      headerInputs[index].set("value", value);
    });
  },
  rowValueHasChanged(rowIndex, rowInput){
    if (!rowInput) {
      return false;
    }

    var defaultRowData = this.get("storedDefaultRowValues");
    if (!defaultRowData[rowIndex]){
      return true
    }
    var defaultValue = defaultRowData[rowIndex].get(rowInput);
    var rowData = this.get("rowData");
    var cell = rowData[rowIndex].get(rowInput);
    return cell.get("value") !== defaultValue
  },
  defaultRowData: computed('row', 'columns', {
    get(key) {
      return this.get("storedDefaultRowValues");
    },
    set(key, rowData) {
      var columnData = A([])
      var columns = this.get("columns");
      rowData = rowData ? rowData : A([]);
      rowData.forEach(function (row) {
        var rowValues = EmberObject.create({});
        columns.forEach(function (column) {
          var rowInput = column.get('input');
          var cell = row.get(rowInput);
          var cellValue = cell.get("value");
          rowValues.set(rowInput, cellValue);
        });
        columnData.push(rowValues);
      });
      this.set("storedDefaultRowValues", columnData);
      return columnData;
    }
  }),
  defaultBulkRowData: computed('headerInputs', {
    get(key) {
      return this.get("storedDefaultBulkRowValues");
    },
    set(key, newHeaderInputsData) {
      var bulkData = A([])
      newHeaderInputsData = newHeaderInputsData ? newHeaderInputsData : A([]);
      newHeaderInputsData.forEach(function (newHeaderInputData) {
        var headerValues = EmberObject.create({});
        var value = newHeaderInputData.get("value");
        var input = newHeaderInputData.get("input");
        headerValues.set("value", value);
        headerValues.set("input", input);
        bulkData.push(headerValues);
      });
      this.set("storedDefaultBulkRowValues", bulkData);
      return bulkData;
    }
  }),

  showUpdateValueDialog(){
    $("#updateValueDialog").modal("show");
  },
  showLockRowsDialog(){
    $("#lockRowsDialog").modal("show");
  },
  showUnlockRowsDialog(){
    $("#unlockRowsDialog").modal("show");
  },
  showBulkUpdateDialog(){
    $("#bulkUpdateValueDialog").modal("show");
  },

  bulkUnlockConfig: computed('bulkUnlockConfirmationConfig', function () {
    return this.getWithDefault("bulkUnlockConfirmationConfig", EmberObject.create({}))
  }),
  bulkLockConfig: computed('bulkLockConfirmationConfig', function () {
    return this.getWithDefault("bulkLockConfirmationConfig", EmberObject.create({}))
  }),
  unlockConfirmationMessage: computed('row.unlockConfirmationContent', function () {
    return this.getWithDefault("row.unlockConfirmationContent", "Are you sure you want to update this value?")
  }),

  unlockRowsConfirmationMessage: computed('bulkUnlockConfig.messageContent', function () {
    return this.getWithDefault("bulkUnlockConfig.messageContent", "Are you sure you want to unlock all rows?")
  }),
  lockRowsConfirmationMessage: computed('bulkLockConfig.messageContent', function () {
    return this.getWithDefault("bulkLockConfig.messageContent", "Are you sure you want to lock all rows?")
  }),

  actions: {
    updateRowData: function (rowIndex, rowInput, requiresConfirmation) {
      if (this.rowValueHasChanged(rowIndex, rowInput) && requiresConfirmation) {
        var rowData = this.get("rowData");
        var cell = rowData[rowIndex].get(rowInput);
        var valueConfirmationConfig = cell.getWithDefault("valueConfirmationConfig", EmberObject.create({}))
        this.set("valueUpdateMessage", valueConfirmationConfig.getWithDefault("messageContent",  "Are you sure you want to update this value?"))
        this.set("valueConfirmationConfig", valueConfirmationConfig);

        this.showUpdateValueDialog();
      } else {
        this.set("defaultRowData", this.get("rowData"));
        this.updateData(this.get("rowData"));
      }
    },
    bulkUpdateChecked: function (column) {

      var disabled = !column.bulkChecked
      column.set('bulkChecked', disabled);
      column.set('value', null);

      var key = column.input

      var rowData = this.get('rowData');
      rowData.forEach(function (item) {
        var rowCell = item.get(key);
        rowCell.set("disabled", disabled)
      });

      if (this.get("bulkUpdateCheckedAction")){
        this.bulkUpdateCheckedAction(); 
      }

    },
    bulkUpdateAction(column){
      var value = column.value;
      var key = column.input;
      var confirmation = column.requiresConfirmation;
      var confirmationConfig = column.bulkConfirmationConfig;
      var actions = this.get("headerInputActions");
      var action;
      if (actions) {
        action = actions[key]
      }
      this.bulkUpdate(key, value, action, confirmation, confirmationConfig)
    },
    dateKeyPress(evt, columnData, fromDate, toDate){
      var pattern = new RegExp(/^[0-9\/]*$/);
      evt = evt || window.event;
      var charCode = evt.keyCode || evt.which;
      var charStr = String.fromCharCode(charCode);
      var passes = charStr.match(pattern);
      if (!passes){
        evt.preventDefault();
      }
    },
    dateFocusOut(column){
      var toDate = column.get("value")
      column.set("value", new Date());
      if (toDate){
        column.set("value", toDate);
      } else{
        column.set("value", "");
      }
    },
    updateDateAction(column, fromDate, toDate){
      var key = column.input;
      var confirmation = column.requiresConfirmation;
      var confirmationConfig = column.bulkConfirmationConfig;
      if (!toDate){
        column.set("value", new Date());
      }
      column.set("value", toDate);
      var actions = this.get("headerInputActions");
      var action;
      if (actions) {
        action = actions[key];
      }
      this.bulkUpdate(key, toDate, action, confirmation, confirmationConfig);
    },
    lockAll: function () {
      if (this.get("unlockOnly")) {
        return;
      }
      if (this.get("bulkLockRequiresConfirmation")) {
        this.showLockRowsDialog();
      } else {
        this.lockAllRows();
      }
    },
    unlockAll: function () {
      if (this.get("bulkLockRequiresConfirmation")) {
        this.showUnlockRowsDialog();
      } else {
        this.unlockAllRows();
      }
    },
    afterConfirmValueChange(){
      this.set("defaultRowData", this.get("rowData"));
      this.updateData(this.get("rowData"));
    },
    afterCancelValueChange(){
      this.revertToDefaultRowValues();
    },
    afterConfirmBulkValueChange(){
      this.set("defaultBulkRowData", this.get("headerInputs"));
      this.performBulkUpdate();
    },
    afterCancelBulkValueChange(){
      this.revertToDefaultBulkRowValues();
    },
    afterConfirmUnlockRows(){
      this.unlockAllRows();
    },
    afterConfirmLockRows(){
      this.lockAllRows();
    }

  }
});
