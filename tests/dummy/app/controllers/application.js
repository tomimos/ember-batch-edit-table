import EmberObject from '@ember/object';
import Controller from '@ember/controller';
import { A } from '@ember/array';
import faker from 'faker';

export default Controller.extend({
  tableConfig: null,
  init() {
    this._super(...arguments);

    var object = EmberObject.extend({});

    var tableConfig = object.create();
    var tableRows = A([]);
    tableConfig.set("lockable", true);
    tableConfig.set("unlockOnly", true);
    tableConfig.set("bulkLockable", false);
    tableConfig.set("bulkLockRequiresConfirmation", false);

    var praticalRows = A([]);
    var sObj = object.create();
    var eObj = object.create();
    sObj.set("id", 1)
    sObj.set("value", "S")
    sObj.set("label", "S")

    eObj.set("id", 2)
    eObj.set("value", "E")
    eObj.set("label", "E")

    praticalRows.push(sObj)
    praticalRows.push(eObj)


    var finalGradeRows = A([]);
    var lowObj = object.create();
    var midObj = object.create();
    var highObj = object.create();

    lowObj.set("id", 1);
    lowObj.set("value", 70);
    lowObj.set("label", "70");

    midObj.set("id", 2);
    midObj.set("value", 75);
    midObj.set("label", "75");

    highObj.set("id", 3);
    highObj.set("value", 80);
    highObj.set("label", "80");

    finalGradeRows.push(lowObj);
    finalGradeRows.push(midObj);
    finalGradeRows.push(highObj);


    var statusRows = A([]);
    var passObj = object.create();
    var failObj = object.create();
    var incompleteOjb = object.create();
    var nullStatObj = object.create();

    passObj.set("id", 1);
    passObj.set("value", "pass");
    passObj.set("label", "Pass");

    failObj.set("id", 2);
    failObj.set("value", "fail");
    failObj.set("label", "Fail");

    incompleteOjb.set("id", 3);
    incompleteOjb.set("value", "incomplete");
    incompleteOjb.set("label", "Incomplete");

    nullStatObj.set("id", 4);
    nullStatObj.set("value", " ");
    nullStatObj.set("label", " ");

    statusRows.push(nullStatObj);
    statusRows.push(passObj);
    statusRows.push(failObj);
    statusRows.push(incompleteOjb);

    var failOnlyOptions = A([]);
    failOnlyOptions.push(nullStatObj);
    failOnlyOptions.push(failObj);
    failOnlyOptions.push(incompleteOjb);

    var statusOptions = object.create({
      allStatusOptions: statusRows,
      failOnlyOptions: failOnlyOptions,
    })
    this.set("gradeStatusOptions", statusOptions);

    var sessionTypeRows = A([]);
    var newSessionObj = object.create();
    var existingSessionObj = object.create();
    newSessionObj.set("id", 1)
    newSessionObj.set("value", "New Session")
    newSessionObj.set("label", "New Session")

    existingSessionObj.set("id", 2)
    existingSessionObj.set("value", "Existing Session")
    existingSessionObj.set("label", "Existing Session")

    sessionTypeRows.push(newSessionObj)
    sessionTypeRows.push(existingSessionObj)


    var sessionRows = A([]);
    var name0Obj = object.create();
    var name1Obj = object.create();
    var name2Obj = object.create();
    var name3Obj = object.create();

    var name1 = faker.name.jobTitle();
    var name2 = faker.name.jobTitle();
    var name3 = faker.name.jobTitle();


    name0Obj.set("value", " ")
    name0Obj.set("label", " ")

    name1Obj.set("id", 1)
    name1Obj.set("value", name1)
    name1Obj.set("label", name1)

    name2Obj.set("id", 2)
    name2Obj.set("value", name2)
    name2Obj.set("label", name2)

    name3Obj.set("id", 3)
    name3Obj.set("value", name3)
    name3Obj.set("label", name3)

    sessionRows.push(name0Obj);
    sessionRows.push(name1Obj);
    sessionRows.push(name2Obj);
    sessionRows.push(name3Obj);

    for (let i = 0; i < 5; i++) {
      var row = object.create();
      var userNameObj = object.create();
      userNameObj.set("additionalClasses", "heyboi");
      userNameObj.set("value", faker.name.lastName() + ", " + faker.name.firstName())
      row.set("studentName", userNameObj);

      var courseNameObj = object.create();
      courseNameObj.set("value", faker.name.jobTitle())
      row.set("courseName", courseNameObj);

      var sessionTypeObj = object.create();
      sessionTypeObj.set("value", existingSessionObj);
      row.set("sessionType", sessionTypeObj);

      var sessionNameObj = object.create();
      sessionNameObj.set("type", "select");
      sessionNameObj.set("value", name1Obj);
      row.set("sessionName", sessionNameObj);


      var testScoreObj = object.create();
      testScoreObj.set("value", (Math.round(Math.random() * 99) + 1) + Math.random())
      testScoreObj.set("postfixSymbol", "%");
      testScoreObj.set("additionalClasses", "test-score-cell");
      row.set("testScore", testScoreObj);

      var finalGradeObj = object.create();
      finalGradeObj.set("value", (Math.round(Math.random() * 99) + 1) + Math.random())
      finalGradeObj.set("postfixSymbol", "%");

      var finalGradeCofigConfirm = object.create();
      finalGradeCofigConfirm.set("messageContent", "<p><strong>All</strong> students who have a 'Final Grade' that is greater than or equal to the 'Final Grade to Pass' and have a Practical set as Satisfactory (S) or Exemplary (E) will have their status set to 'Pass' and their date completed set to the date specified above.</p><p> Those that Do Not Meet the new Final Grade to Pass, that have already been set to pass and are not locked, will be set to 'blank' (no status). <strong>They will lose their status of Passing the Course.</strong></p><p> Users without a Passing Final Grade and/or Pracitical of either 'S' or 'E', and/or are locked will not be effected.</p>")
      finalGradeCofigConfirm.set("showCheckbox", true)
      finalGradeCofigConfirm.set("checkboxMessage", "I understand the impact of this change.")
      finalGradeCofigConfirm.set("headerIconColor", "red")
      finalGradeCofigConfirm.set("headerIconClass", "fa-exclamation-triangle")
      finalGradeCofigConfirm.set("headerText", "Changing the Passing Grade May Impact the Status of the Listed Users")
      finalGradeCofigConfirm.set("headerTextColor", "black")
      finalGradeCofigConfirm.set("titleIconColor", "red")
      finalGradeCofigConfirm.set("titleIconClass", "fa-exclamation-triangle")
      finalGradeCofigConfirm.set("titleText", "IMPORTANT")
      finalGradeCofigConfirm.set("titleTextColor", "black")
      finalGradeCofigConfirm.set("confirmText", "Make Change")

      finalGradeObj.set("valueConfirmationConfig", finalGradeCofigConfirm)

      row.set("finalGrade", finalGradeObj);

      var courseStatusObj = object.create();
      courseStatusObj.set("value", "")
      courseStatusObj.set("iconClass", "fa-ban")
      courseStatusObj.set("iconColor", "red")
      row.set("courseStatus", courseStatusObj);

      var lmsScoreObj = object.create();
      lmsScoreObj.set("value", "")
      lmsScoreObj.set("iconClass", "fa-ban")
      lmsScoreObj.set("iconColor", "red")
      row.set("lmsScore", lmsScoreObj);

      var testGeneratorObj = object.create();
      testGeneratorObj.set("value", "")
      testGeneratorObj.set("iconClass", "fa-ban")
      testGeneratorObj.set("iconColor", "red")
      row.set("testGenerator", testGeneratorObj);


      var praticalObj = object.create();
      praticalObj.set("value", eObj)
      row.set("practical", praticalObj);

      var statusObj = object.create();

      statusObj.set("value", passObj)
      row.set("status", statusObj);


      var startDateObj = object.create();
      startDateObj.set("icon", "fa-calendar date-user")
      startDateObj.set("dateInputClass", "date-input")
      startDateObj.set("value", faker.date.future())
      row.set("startDate", startDateObj);

      if (i % 2) {
        row.set("additionalClasses", "testclass1 testclass2");
      } else {
        row.set("locked", true);
      }

      var unlockConfig = object.create();
      unlockConfig.set("messageContent", "<p>This users completions/grade record for this course was previously set. Unlocking this record will allow you to edit this user's completion/grade for this course and will override the existing record.</p>")
      unlockConfig.set("showCheckbox", true)
      unlockConfig.set("checkboxMessage", "I understand the impact of this change.")
      unlockConfig.set("headerIconColor", "red")
      unlockConfig.set("headerIconClass", "fa-exclamation-triangle")
      unlockConfig.set("headerText", "Unlocking this record will override the existing record.")
      unlockConfig.set("headerTextColor", "black")
      unlockConfig.set("titleIconColor", "red")
      unlockConfig.set("titleIconClass", "fa-exclamation-triangle")
      unlockConfig.set("titleText", "IMPORTANT")
      unlockConfig.set("titleTextColor", "black")
      unlockConfig.set("confirmText", "Make Change")

      row.set("lockRequiresConfirmation", true)
      row.set("unlockConfig", unlockConfig);

      tableRows.push(row);
    }

    tableConfig.set("rowData", tableRows);

    var tableConfigColumns = A([]);
    var userNameConfig = object.create();
    userNameConfig.set("input", "studentName");
    userNameConfig.set("label", "Student")
    userNameConfig.set("bulkEditable", false);
    tableConfigColumns.push(userNameConfig);

    var courseNameConfig = object.create();
    courseNameConfig.set("input", "courseName");
    courseNameConfig.set("label", "Course")
    courseNameConfig.set("bulkEditable", false);
    tableConfigColumns.push(courseNameConfig);

    var sessionTypeConfig = object.create();
    sessionTypeConfig.set("input", "sessionType");
    sessionTypeConfig.set("label", "Session Type")
    sessionTypeConfig.set("type", "select");
    sessionTypeConfig.set("bulkEditable", false);
    sessionTypeConfig.set("content", sessionTypeRows)
    sessionTypeConfig.set("optionValuePath", "content.value")
    sessionTypeConfig.set("optionLabelPath", "content.label")
    tableConfigColumns.push(sessionTypeConfig);

    var sessionNameConfig = object.create();
    sessionNameConfig.set("input", "sessionName");
    sessionNameConfig.set("label", "Session Name")
    sessionNameConfig.set("bulkEditable", false);
    sessionNameConfig.set("content", sessionRows)
    sessionNameConfig.set("optionValuePath", "content.value")
    sessionNameConfig.set("optionLabelPath", "content.label")
    tableConfigColumns.push(sessionNameConfig);

    var courseStatusConfig = object.create();
    courseStatusConfig.set("input", "courseStatus");
    courseStatusConfig.set("label", "LMS Course Status")
    courseStatusConfig.set("bulkEditable", false);
    tableConfigColumns.push(courseStatusConfig);

    var lmsScoreConfig = object.create();
    lmsScoreConfig.set("input", "lmsScore");
    lmsScoreConfig.set("label", "LMS Score (Avg)")
    lmsScoreConfig.set("bulkEditable", false);
    tableConfigColumns.push(lmsScoreConfig);

    var testGeneratorConfig = object.create();
    testGeneratorConfig.set("input", "testGenerator");
    testGeneratorConfig.set("label", "Test Generator")
    testGeneratorConfig.set("bulkEditable", false);
    testGeneratorConfig.set("verticalTableBreakAfter", true);
    tableConfigColumns.push(testGeneratorConfig);

    var testScoreConfig = object.create();
    testScoreConfig.set("input", "testScore");
    testScoreConfig.set("label", "Test Score")
    testScoreConfig.set("type", "float");
    testScoreConfig.set("fixedDecimalPlaces", 1);
    testScoreConfig.set("min", 0);
    testScoreConfig.set("max", 100);
    testScoreConfig.set("bulkEditable", false);
    testScoreConfig.set("additionalClasses", "test-score-header-cell");
    tableConfigColumns.push(testScoreConfig);

    var finalGradeCofig = object.create();
    finalGradeCofig.set("input", "finalGrade");
    finalGradeCofig.set("label", "Final Grade")
    finalGradeCofig.set("type", "float");
    finalGradeCofig.set("min", 0);
    finalGradeCofig.set("max", 100);
    finalGradeCofig.set("fixedDecimalPlaces", 1);
    finalGradeCofig.set("bulkEditable", false);
    finalGradeCofig.set("requiresConfirmation", false);
    tableConfigColumns.push(finalGradeCofig);

    var praticalConfig = object.create();
    praticalConfig.set("input", "practical");
    praticalConfig.set("label", "Practical")
    praticalConfig.set("type", "select");
    praticalConfig.set("bulkEditable", false);
    praticalConfig.set("content", praticalRows)
    praticalConfig.set("optionValuePath", "content.value")
    praticalConfig.set("optionLabelPath", "content.label")
    tableConfigColumns.push(praticalConfig);

    var startDateConfig = object.create();
    startDateConfig.set("type", "date");
    startDateConfig.set("input", "startDate");
    startDateConfig.set("type", "date");
    startDateConfig.set("placeHolder", "Start Date");
    startDateConfig.set("label", "Access Start Date");
    startDateConfig.set("bulkEditable", true);
    startDateConfig.set("requiresConfirmation", false);


    var statusConfig = object.create();
    statusConfig.set("input", "status");
    statusConfig.set("label", "Status")
    statusConfig.set("type", "select");
    statusConfig.set("bulkEditable", false);
    statusConfig.set("content", statusRows)
    statusConfig.set("optionValuePath", "content.value")
    statusConfig.set("optionLabelPath", "content.label")
    tableConfigColumns.push(statusConfig);

    tableConfigColumns.push(startDateConfig);
    tableConfig.set("columns", tableConfigColumns);

    var tableHeaderInputs = A([]);
    var passedConfig = object.create();
    passedConfig.set("input", "finalGrade");
    passedConfig.set("label", "Final Grade to Pass")
    passedConfig.set("type", "select");
    passedConfig.set("content", finalGradeRows);
    passedConfig.set("optionValuePath", "content.value");
    passedConfig.set("optionLabelPath", "content.label");
    passedConfig.set("value", midObj);
    passedConfig.set("requiresConfirmation", true);

    var bulkConfirmationConfig = object.create();
    bulkConfirmationConfig.set("messageContent", "<p><strong>All</strong> students who have a 'Final Grade' that is greater than or equal to the 'Final Grade to Pass' and have a Practical set as Satisfactory (S) or Exemplary (E) will have their status set to 'Pass' and their date completed set to the date specified above.</p><p> Those that Do Not Meet the new Final Grade to Pass, that have already been set to pass and are not locked, will be set to 'blank' (no status). <strong>They will lose their status of Passing the Course.</strong></p><p> Users without a Passing Final Grade and/or Pracitical of either 'S' or 'E', and/or are locked will not be effected.</p>")
    bulkConfirmationConfig.set("showCheckbox", true)
    bulkConfirmationConfig.set("checkboxMessage", "I understand the impact of this change.")
    bulkConfirmationConfig.set("headerIconColor", "red")
    bulkConfirmationConfig.set("headerIconClass", "fa-exclamation-triangle")
    bulkConfirmationConfig.set("headerText", "Changing the Passing Grade May Impact the Status of the Listed Users")
    bulkConfirmationConfig.set("headerTextColor", "black")
    bulkConfirmationConfig.set("titleIconColor", "red")
    bulkConfirmationConfig.set("titleIconClass", "fa-exclamation-triangle")
    bulkConfirmationConfig.set("titleText", "IMPORTANT")
    bulkConfirmationConfig.set("titleTextColor", "black")
    bulkConfirmationConfig.set("confirmText", "Make Change")

    passedConfig.set("bulkConfirmationConfig", bulkConfirmationConfig)

    this.set("gradeToPass", midObj.value);


    tableHeaderInputs.push(passedConfig);
    tableConfig.set("headerInputs", tableHeaderInputs);

    var dateCompletedConfig = object.create();
    dateCompletedConfig.set("label", "Date Completed");
    dateCompletedConfig.set("input", "startDate");
    dateCompletedConfig.set("type", "date");
    tableHeaderInputs.push(dateCompletedConfig);
    tableConfig.set("headerInputs", tableHeaderInputs);
    tableConfig.set("observedProperties", A(["row.sessionName.type", "row.status.content"]))

    this.set("tableConfig", tableConfig);
  },
  processData(data, forcePass) {
    var statusOptions = this.get("gradeStatusOptions");
    var gradeToPass = this.get("gradeToPass");

    var emptyOption = statusOptions.allStatusOptions[0];
    var passOption = statusOptions.allStatusOptions[1];
    var failOption = statusOptions.allStatusOptions[2];
    var incompleteOption = statusOptions.allStatusOptions[3];

    data.forEach(function(item) {
      var finalGradeCell = item.get("finalGrade");
      var practicalCell = item.get("practical");
      var statusCell = item.get("status");
      var statusValue = statusCell.get("value");
      var finalGradeValue = finalGradeCell.get("value");
      var practicalValue = practicalCell.get("value");

      if (finalGradeValue >= gradeToPass && practicalValue === "S") {
        statusCell.set("content", statusOptions.allStatusOptions);
        if (forcePass && (statusValue !== incompleteOption.value)) {
          statusCell.set("value", passOption.value);
        }
      } else {
        statusCell.set("content", statusOptions.failOnlyOptions);
        if (statusValue !== incompleteOption.value) {
          statusCell.set("value", failOption.value);
        }
      }
    })
  },

 


  actions: {
    updateData(data) {
      this.processData(data);
    },

    bulkUpdateCheckedAction() {
    },

    
    passedCourse: function(selection) {
      this.set("gradeToPass", selection.value);
      this.processData(this.tableConfig.rowData, true)
    }
  }
});
